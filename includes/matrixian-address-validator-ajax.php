<?php
/**
 * Mimic the actual admin-ajax.
 */
define( 'DOING_AJAX', true );

if ( ! isset( $_POST['action'] ) ) {
	die( '-1' );
}

ini_set( 'html_errors', 0 );

// make sure we skip most of the loading which we might not need
// http://core.trac.wordpress.org/browser/branches/3.4/wp-settings.php#L99
define( 'SHORTINIT', true );

// make sure you update this line
// to the relative location of the wp-load.php
require_once '../../../../wp-load.php';

// Typical headers
header( 'Content-Type: application/json' );
send_nosniff_header();

/**
 * Spin up WordPress.
 * Include only the files and function we need.
 */ 
require_once ABSPATH . WPINC . '/default-constants.php';

wp_plugin_directory_constants();

require_once ABSPATH . WPINC . '/class-wp-hook.php';
require_once ABSPATH . WPINC . '/class-wp-session-tokens.php';
require_once ABSPATH . WPINC . '/class-wp-user-meta-session-tokens.php';
require_once ABSPATH . WPINC . '/class-wp-user.php';
require_once ABSPATH . WPINC . '/class-wp-roles.php';
require_once ABSPATH . WPINC . '/class-wp-role.php';

wp_cookie_constants();

require_once ABSPATH . WPINC . '/formatting.php';
require_once ABSPATH . WPINC . '/capabilities.php';
require_once ABSPATH . WPINC . '/query.php'; // - might be useful
require_once ABSPATH . WPINC . '/l10n.php';
require_once ABSPATH . WPINC . '/user.php';
require_once ABSPATH . WPINC . '/meta.php';
require_once ABSPATH . WPINC . '/rest-api.php';
require_once ABSPATH . WPINC . '/http.php';
require_once ABSPATH . WPINC . '/theme.php';

require_once ABSPATH . WPINC . '/general-template.php';
require_once ABSPATH . WPINC . '/link-template.php';

wp_templating_constants();

require_once ABSPATH . WPINC . '/vars.php';
require_once ABSPATH . WPINC . '/kses.php';
require_once ABSPATH . WPINC . '/pluggable.php';
require_once ABSPATH . WPINC . '/cron.php';

/**
 * Spin up our plugin.
 */
global $wp_plugin_paths;
$wp_plugin_paths = array();

foreach ( wp_get_active_and_valid_plugins() as $plugin ) {
	if ( strpos( $plugin, 'matrixian-address-validator' ) !== false ) {
		wp_register_plugin_realpath( $plugin );
		include_once $plugin;
		do_action( 'plugin_loaded', $plugin );
	}
}

/**
 * Execute our AJAX functions.
 */ 
$action = esc_attr( trim( $_POST['action'] ) );

// A bit of security.
$allowed_actions = array(
	'validate_address',
);

if ( in_array( $action, $allowed_actions ) ) {
	if ( is_user_logged_in() ) {
		do_action( 'wp_ajax_' . $action );
	} else {
		do_action( 'wp_ajax_nopriv_' . $action );
	}
} else {
	die( '-1' );
}
