<?php

/**
 * The api-specific functionality of the plugin.
 *
 * @link       https://www.matrixiangroup.com/en/
 * @since      1.0.0
 *
 * @package    Matrixian_Address_Validator
 * @subpackage Matrixian_Address_Validator/admin
 */

/**
 * The api-specific functionality of the plugin.
 *
 * Defines the API endpoints to connect to the Matrixian Group API.
 *
 * @package    Matrixian_Address_Validator
 * @subpackage Matrixian_Address_Validator/admin
 */
class Matrixian_Address_Validator_API {

	/**
	 * The stored API key file path.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $key_file_path    The relative API key file path
	 */
	private $key_file_path;

	/**
	 * The API base endpoint of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $base_endpoint_api    The host of this plugin's API.
	 */
	private $base_endpoint_api;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->key_file_path     = plugin_dir_path( __DIR__ ) . '/includes/auth-key-file.json';
		$this->base_endpoint_api = 'https://api.matrixiangroup.com';

	}

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string $username    Username for Matrixian Group API.
	 * @param    string $password    Password for Matrixian Group API.
	 * @return
	 */
	public function get_auth_token( $username, $password ) {

		try {

			// Check if API has been enabled.
			if ( get_option( 'matrixian_enabled' ) !== 'yes' ) {

				throw new Exception( __( 'You must enable the Matrixian Group API to validate addresses. Go to WooCommerce -> Advanced -> Matrixian Address Validator to enable the API.', 'matrixian-address-validator' ) );

			}

			// Check if retrieved token exists.
			if ( file_exists( $this->key_file_path ) ) {

				$token_file = file_get_contents( $this->key_file_path );
				$token_json = json_decode( $token_file, true );

				// Check if token has not been expired.
				if ( date( 'Ymd' ) < date( 'Ymd', strtotime( $token_json['.expires'] ) ) ) {

					return $token_json['access_token'];

				}
			}

			if ( $username === '' || $password === '' ) {

				throw new Exception( __( 'Username and/or password for API not set. Go to WooCommerce -> Advanced -> Matrixian Address Validator to provide the API credentials.', 'matrixian-address-validator' ) );

			}

			// Stored token no longer valid or not set. Get new one.
			$curl = curl_init();

			curl_setopt_array(
				$curl,
				array(
					CURLOPT_URL            => $this->base_endpoint_api . '/token',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING       => '',
					CURLOPT_MAXREDIRS      => 10,
					CURLOPT_TIMEOUT        => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST  => 'POST',
					CURLOPT_POSTFIELDS     => array(
						'username' => $username,
						'password' => $password,
					),
				)
			);

			$response    = curl_exec( $curl );
			$http_status = intval( curl_getinfo( $curl, CURLINFO_HTTP_CODE ) );
			if ( 200 !== $http_status ) {
				$errno = curl_errno( $curl );
				$error = curl_error( $curl );
				curl_close( $curl );

				$error_message = sprintf(
					__(
						'HTTP Status %1$s encountered while attempting to retrieve an access key. Error (%2$s): %3$s.',
						'matrixian-address-validator'
					),
					$http_status,
					$errno,
					$error
				);

				throw new \Exception( $error_message );
			}

			file_put_contents( $this->key_file_path, $response );
			$json_token = json_decode( $response, true );

			curl_close( $curl );

			return $json_token['access_token'];

		} catch ( \Exception $e ) {

			// Handle occurred exception.
			plugin_handle_exceptions( $e );

		}

	}

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function validate_address() {

		try {

			// Check if API has been enabled.
			if ( get_option( 'matrixian_enabled' ) !== 'yes' ) {

				throw new Exception( __( 'You must enable the Matrixian Group API to validate addresses. Go to WooCommerce -> Advanced -> Matrixian Address Validator to enable the API.', 'matrixian-address-validator' ) );

			}

			// Check if user credentials are filled out.
			if ( ! get_option( 'matrixian_api_user' ) || ! get_option( 'matrixian_api_password' ) ) {

				throw new Exception( __( 'Username and/or password for API not set. Go to WooCommerce -> Advanced -> Matrixian Address Validator to provide the API credentials.', 'matrixian-address-validator' ) );

			}

			// Get access token.
			$access_token  = $this->get_auth_token( get_option( 'matrixian_api_user' ), get_option( 'matrixian_api_password' ) );
			$address_data  = $_POST;
			$address_items = get_address_components( $address_data['address'] );

			error_log( print_r( $address_items, true ) );

			$url_params = array(
				'countryCode'    => 'country_code',
				'street'         => 'streetName',
				'houseNumber'    => 'houseNumberParts',
				'houseNumberExt' => 'address_ext',
				'postalCode'     => 'postal_code',
				'city'           => 'city',
			);

			$url_pairs = array();
			foreach ( $url_params as $key => $param ) {
				if ( $key === 'street' ) {
					$url_pairs[ $key ] = ( isset( $address_items[ $param ] ) ? $address_items[ $param ] : '****' );
				} elseif ( $key === 'houseNumber' ) {
					$housenumber       = ( isset( $address_items['houseNumberParts']['base'] ) ? $address_items['houseNumberParts']['base'] : '' );
					$housenumber      .= ( isset( $address_items['houseNumberParts']['extension'] ) ? $address_items['houseNumberParts']['extension'] : '' );
					$url_pairs[ $key ] = $housenumber ?: '****';
				} else {
					$url_pairs[ $key ] = ( $address_data[ $param ] ?: '****' );
				}
			}

			if ( ! $address_data['address_ext'] ) {
				unset( $url_pairs['houseNumberExt'] );
			}

			$url = $this->base_endpoint_api . '/address/check?' . http_build_query( $url_pairs, '', '&' );

			$curl = curl_init();
			curl_setopt_array(
				$curl,
				array(
					CURLOPT_URL            => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING       => '',
					CURLOPT_MAXREDIRS      => 10,
					CURLOPT_TIMEOUT        => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST  => 'GET',
					CURLOPT_HTTPHEADER     => array(
						'Authorization: Bearer ' . $access_token,
					),
				)
			);

			$response    = curl_exec( $curl );
			$http_status = intval( curl_getinfo( $curl, CURLINFO_HTTP_CODE ) );
			if ( 200 !== $http_status ) {
				$errno = curl_errno( $curl );
				$error = curl_error( $curl );
				curl_close( $curl );

				$error_message = sprintf(
					__(
						'HTTP Status %1$s encountered while attempting to validate a given address. Error (%2$s): %3$s.',
						'matrixian-address-validator'
					),
					$http_status,
					$errno,
					$error
				);

				throw new \Exception( $error_message );
			}

			curl_close( $curl );

			echo $response;
			die();

		} catch ( \Exception $e ) {

			// Handle occurred exception.
			plugin_handle_exceptions( $e );

		}
	}
}
