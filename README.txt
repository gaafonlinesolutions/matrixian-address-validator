=== Matrixian Address Validator ===
Contributors: gaafdigitalagency, matrixian
Donate link: https://www.matrixiangroup.com/en/
Tags: address, address validation, billing, validation, woocommerce
Requires PHP: 7.0
Requires at least: 5.0
Tested up to: 5.9.3
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Address Validator for WooCommerce.

== Description ==

Address Validator for WooCommerce

The WooCommerce Address Validator plugin uses the [Matrixian Address Validator API](https://platform.matrixiangroup.com/api/documentation "Matrixian API documentation") to validate and correct billing/shipping addresses for the following countries:

* Austria
* Belgium
* Czech Republic
* Denmark
* Germany
* France
* Italy
* Luxembourg
* Spain
* Sweden
* The Netherlands
* United Kingdom
* United States

Stop wasting time and money with undelivered packages or having to reach out to customers to get the correct addresses.

SUPPORTED PLUGINS / COMPATIBLE FORMS

The plugin integrates seamlessly with:

* WooCommerce

KEY FEATURES

Automatic validation of billing and shipping addresses
Customers get a real-time suggestion if they enter an incomplete address
Easy-to-use, automatically integrates with WooCommerce checkout pages

GETTING STARTED

The plugin requires a Matrixian Group Platform account. You can register at [Matrixian Platform](https://platform.matrixiangroup.com/register "Matrixian Platform registration")


== Installation ==

1. Upload `matrixian-address-validator.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to the Settings page and enable the API
4. Fill out your Matrixian account credentials
5. Address Validator is now active


== Frequently Asked Questions ==

= Does it include auto-complete functionality? =

No, this plugin is used for address validation purposes only.

= Does it work for my country? =

We do not support all countries at the moment, but we will add support for new countries in the future. You can find a list of supported countries in the plugin description.

= Is this plugin free to use? =

No, this plugin requires an Matrixian Platform account. You will be charged based on the amount of validations you run. 

= Where can I get a Matrixian account? =

You can register at [Matrixian Platform](https://platform.matrixiangroup.com/register "Matrixian Platform registration")

== Screenshots ==

1. Settings.jpg
2. Invalid address.jpg
3. Valid address.jpg

== Changelog ==

= 1.0 =
* Support validation for all listed WooCommerce countries.
* Support address validation for billing and shipping addresses.
* Initial version.

== Upgrade Notice ==

= 1.0 =
Add address validation to default WooCommerce billing and shipping fields for all listed countries.
